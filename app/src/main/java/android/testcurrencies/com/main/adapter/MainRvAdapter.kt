package android.testcurrencies.com.main.adapter

import android.testcurrencies.com.R
import android.testcurrencies.com.model.CurrencyModel
import android.testcurrencies.com.skeleton.recycler.BaseRvAdapter
import android.testcurrencies.com.skeleton.recycler.BaseVH
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_main_currency.view.*

/**
 * Created by Koba on 24.10.2018.
 * cosmicgate97@gmail.com
 */

class MainRvAdapter : BaseRvAdapter<CurrencyModel, BaseVH<CurrencyModel?>>() {

    companion object {
        const val ITEM_HEADER = 1
        const val ITEM_CURRENCY = 2
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0)
            ITEM_HEADER
        else ITEM_CURRENCY
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseVH<CurrencyModel?> {
        return if (viewType == ITEM_HEADER)
            HeaderViewHolder(parent)
        else ViewHolder(parent)

    }

    inner class ViewHolder(parent: ViewGroup) : BaseVH<CurrencyModel?>(parent, R.layout.item_main_currency) {

        override fun bind(item: CurrencyModel?) {
            itemView.itemCurrencyName.text = item?.name
            itemView.itemCurrencyVolume.text = item?.volume.toString()
            itemView.itemCurrencyAmount.text = String.format("%.2f", item?.price?.amount ?: 0f)
        }
    }

    inner class HeaderViewHolder(parent: ViewGroup) : BaseVH<CurrencyModel?>(parent, R.layout.item_main_currency_header) {

        override fun bind(item: CurrencyModel?) {
        }
    }
}
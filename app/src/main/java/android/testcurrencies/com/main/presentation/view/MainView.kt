package android.testcurrencies.com.main.presentation.view

import android.testcurrencies.com.model.CurrencyModel
import android.testcurrencies.com.skeleton.presentation.BaseView

/**
 * Created by Koba on 24.10.2018.
 * cosmicgate97@gmail.com
 */

interface MainView : BaseView {

    fun setEmptyState()
    fun setCurrenciesList(items: ArrayList<CurrencyModel>)

}

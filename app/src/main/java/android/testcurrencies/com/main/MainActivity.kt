package android.testcurrencies.com.main

import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.testcurrencies.com.App.Companion.context
import android.testcurrencies.com.main.presentation.view.MainView
import android.testcurrencies.com.main.presentation.presenter.MainPresenter
import android.testcurrencies.com.R
import android.testcurrencies.com.main.adapter.MainRvAdapter
import android.testcurrencies.com.model.CurrencyModel
import android.testcurrencies.com.skeleton.activity.BaseActivity
import android.view.Menu
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import android.view.MenuItem
import timber.log.Timber
import java.util.*

/**
 * Created by Koba on 24.10.2018.
 * cosmicgate97@gmail.com
 */

class MainActivity : BaseActivity<MainPresenter>(), MainView {

    companion object {
        const val TIMER_DELAY = 15000L
    }

    lateinit var mainRvAdapter: MainRvAdapter

    lateinit var timer: Timer
    lateinit var timerTask: TimerTask

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun createPresenter(): MainPresenter = MainPresenter(this)

    override fun initViews() {
        mainRvAdapter = MainRvAdapter()
        mainRv.layoutManager = LinearLayoutManager(context)
        mainRv.addItemDecoration(
            DividerItemDecoration(
                context!!,
                DividerItemDecoration.VERTICAL
            )
        )
        mainRv.adapter = mainRvAdapter
        presenter.getCurrenciesList()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_refresh -> {
                presenter.getCurrenciesList()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        try {
            timer = Timer()
            timerTask = object : TimerTask() {
                override fun run() {
                    presenter.getCurrenciesList()
                }
            }
            timer.schedule(timerTask, TIMER_DELAY, TIMER_DELAY)
        } catch (e: IllegalStateException) {
            Timber.e(e)
        }
    }

    override fun onPause() {
        super.onPause()
        timer.cancel()
    }

    override fun setEmptyState() {
        mainTvEmpty.visibility = View.VISIBLE
        mainRv.visibility = View.GONE
    }

    override fun setCurrenciesList(items: ArrayList<CurrencyModel>) {
        mainRv.visibility = View.VISIBLE
        mainTvEmpty.visibility = View.GONE
        mainRvAdapter.setItems(items)
    }
}

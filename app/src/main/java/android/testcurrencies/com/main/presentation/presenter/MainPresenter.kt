package android.testcurrencies.com.main.presentation.presenter

import android.testcurrencies.com.api.RetrofitService
import android.testcurrencies.com.api.responses.CurrencyResponse
import android.testcurrencies.com.main.presentation.view.MainView
import android.testcurrencies.com.model.CurrencyModel
import android.testcurrencies.com.skeleton.presentation.BasePresenter
import retrofit2.Callback
import timber.log.Timber
import retrofit2.Call
import retrofit2.Response

/**
 * Created by Koba on 24.10.2018.
 * cosmicgate97@gmail.com
 */

class MainPresenter(override val view: MainView) : BasePresenter() {

    fun init() {
    }

    fun getCurrenciesList() {
        view.showProgressView()
        RetrofitService.getService().getCurrencies().enqueue(object : Callback<CurrencyResponse> {
            override fun onResponse(call: Call<CurrencyResponse>, response: Response<CurrencyResponse>) {
                if (response.isSuccessful && response.body() != null && response.body()?.stock?.isNotEmpty() == true) {
                    // tasks available
                    view.hideProgressView()
                    val items : ArrayList<CurrencyModel> = ArrayList()
                    items.add(CurrencyModel(null, null, null))
                    items.addAll(response.body()?.stock!!)
                    view.setCurrenciesList(items)

                } else {
                    view.hideProgressView()
                    view.setEmptyState()
                    Timber.e("Error")
                }
            }

            override fun onFailure(call: Call<CurrencyResponse>, t: Throwable) {
                view.hideProgressView()
                view.setEmptyState()
                Timber.e("Error -> ${t.message}")
            }
        })
    }
}

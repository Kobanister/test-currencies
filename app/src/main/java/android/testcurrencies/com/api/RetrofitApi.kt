package android.testcurrencies.com.api

import android.testcurrencies.com.api.responses.CurrencyResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Created by Koba on 24.10.2018.
 * cosmicgate97@gmail.com
 */

interface RetrofitApi {

    @GET("stocks.json")
    fun getCurrencies(): Call<CurrencyResponse>

}
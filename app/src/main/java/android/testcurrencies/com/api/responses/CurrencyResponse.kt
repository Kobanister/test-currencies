package android.testcurrencies.com.api.responses

import android.testcurrencies.com.model.CurrencyModel


/**
 * Created by Koba on 24.10.2018.
 * cosmicgate97@gmail.com
 */

class CurrencyResponse (var stock: List<CurrencyModel>)
package android.testcurrencies.com.splash

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.testcurrencies.com.main.MainActivity
import android.content.Intent
import android.testcurrencies.com.R

/**
 * Created by Koba on 24.10.2018.
 * cosmicgate97@gmail.com
 */
class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        startActivity(Intent(applicationContext, MainActivity::class.java))
        finish()
    }
}
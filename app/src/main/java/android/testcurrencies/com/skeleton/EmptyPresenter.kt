package android.testcurrencies.com.skeleton

import android.testcurrencies.com.skeleton.presentation.BasePresenter
import android.testcurrencies.com.skeleton.presentation.BaseView

class EmptyPresenter(override val view: BaseView) : BasePresenter()

package android.testcurrencies.com.skeleton.presentation

abstract class BasePresenter {

   abstract val view: BaseView
}

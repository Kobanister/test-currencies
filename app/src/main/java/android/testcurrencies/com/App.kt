package android.testcurrencies.com

import android.app.Application

import android.content.Context
import android.content.res.Resources
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        Timber.plant(Timber.DebugTree())
    }

    companion object {

        @Volatile private var instance: App? = null

        val context: Context
            get() = instance!!.applicationContext
    }

}
